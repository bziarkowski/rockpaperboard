﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoardField : MonoBehaviour
{
    public List<BoardField> boardConnected = new List<BoardField>();
    public Button button;

    public BoardPlayer playerOn;

    public bool attack;
    public bool health;
    public bool home;
    public bool boss;
    public bool coins;
    public bool shop;

    public GameObject effectsText;

    private void Awake()
    {
        button = GetComponent<Button>();
    }

    public void SetClear()
    {
        GetComponent<Image>().sprite = GameManager.instance.clearField;
        attack = false;
        health = false;
    }
}
