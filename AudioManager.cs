﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    public AudioSource effectsSource;
    public AudioSource musicSource;
    public AudioSource fightSource;

    public AudioClip rock;
    public AudioClip paper;
    public AudioClip scissors;
    public AudioClip pistol;
    public AudioClip handSlide;
    public AudioClip fightLost;
    public AudioClip fightWon;
    public AudioClip bossBeaten;
    public AudioClip fightStart;
    public AudioClip bossFightStart;
    public AudioClip boardMove;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    public void PlayClip(AudioClip clip)
    {
        effectsSource.PlayOneShot(clip);
    }
}
