﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy
{
    public int maxHP;
    public int currentHP;

    public int combo;

    public int dmg;

    public bool gunUsed;

    public Enemy (int hp, int dmg)
    {
        this.currentHP = hp;
        this.maxHP = hp;
        this.dmg = dmg;
        this.gunUsed = false;
    }

    public void Reset()
    {
        gunUsed = false;
    }
}
