﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoardUI : MonoBehaviour
{
    public static BoardUI instance;

    public BoardPlayer player1;
    public BoardPlayer player2;

    public Slider player1Slider;
    public Text player1Text;
    public Text player1Combo;
    public Text player1Coins;
    public Slider player2Slider;
    public Text player2Text;
    public Text player2Combo;
    public Text player2Coins;

    public void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        UpdatePlayersUI();
    }

    public void UpdatePlayersUI()
    {
        player1Slider.maxValue = player1.maxHP;
        player1Slider.value = player1.currentHP;
        player1Combo.text = "Atk " + player1.dmg;
        player1Text.text = "HP: " + player1.currentHP + "/" + player1.maxHP;
        player1Coins.text = "Monety: " + player1.coins;

        player2Slider.maxValue = player2.maxHP;
        player2Slider.value = player2.currentHP;
        player2Combo.text = "Atk " + player2.dmg;
        player2Text.text = "HP: " + player2.currentHP + "/" + player2.maxHP;
        player2Coins.text = "Monety: " + player2.coins;
    }
}
