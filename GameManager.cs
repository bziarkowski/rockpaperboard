﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public List<BoardField> boardFields = new List<BoardField>();
    public bool fighting;
    public List<BoardPlayer> boardPlayers = new List<BoardPlayer>();

    public Text boardMessage;
    public Text turnCount;

    public int turn;

    public Sprite clearField;

    public GameObject gameWinMessage;
    public Text gameWinText;

    public int bossHP;
    public int bossDmg;
    public Text bossStatsText;

    public GameObject instructions;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        if(SceneManager.GetActiveScene().name == "BoardGame")
        {
            if(PlayerPrefs.GetInt("PlayedCount") == 0)
            {
                instructions.SetActive(true);
                PlayerPrefs.SetInt("PlayedCount", 1);
            }
        }
        DrawEnemyStats();
        StartBoardTurn();
    }

    public void CloseInstructions()
    {
        instructions.SetActive(false);
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void DrawEnemyStats()
    {
        bossHP = Random.Range(8, 14);
        bossDmg = Random.Range(1, 3);
        bossStatsText.text = "HP: " + bossHP + ", Atk " + bossDmg;
    }

    public void StartBoardTurn()
    {
        turn += 1;
        turnCount.text = "Tura " + turn;
        boardPlayers[0].ActivateBoardFields(boardPlayers[1].GetComponent<Image>(), "Tura Gracza #1");
        boardPlayers[0].afterMoveAction = () => boardPlayers[1].ActivateBoardFields(boardPlayers[0].GetComponent<Image>(), "Tura Gracza #2");
        boardPlayers[1].afterMoveAction = () => StartBoardTurn();
    }

    public void GameWinMessage(int playerIndex)
    {
        gameWinMessage.SetActive(true);
        gameWinText.text = "Gracz #" + playerIndex + " zwyciężył!";
    }
}
