﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Shop : MonoBehaviour
{
    public static Shop instance;

    public GameObject panel;

    public Text coins;
    public Button healthButton;
    public Button attackButton;

    public BoardPlayer currentBoardPlayer;
    public UnityAction closeAction;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    public void EnterShop(BoardPlayer player, UnityAction closeAction)
    {
        panel.SetActive(true);

        this.closeAction = closeAction;
        currentBoardPlayer = player;

        coins.text = "Monety: " + player.coins;

        if(player.coins >= 20)
        {
            healthButton.gameObject.SetActive(true);
            attackButton.gameObject.SetActive(true);
        }
        else
        {
            healthButton.gameObject.SetActive(false);
            attackButton.gameObject.SetActive(false);
        }
    }

    public void BuyHealth()
    {
        currentBoardPlayer.coins -= 20;
        currentBoardPlayer.maxHP += 4;
        currentBoardPlayer.currentHP += 4;
        EnterShop(currentBoardPlayer, closeAction);
    }

    public void BuyAttack()
    {
        currentBoardPlayer.coins -= 20;
        currentBoardPlayer.dmg += 2;
        EnterShop(currentBoardPlayer, closeAction);
    }

    public void LeaveShop()
    {
        closeAction();
        panel.SetActive(false);
        BoardUI.instance.UpdatePlayersUI();
    }
}
