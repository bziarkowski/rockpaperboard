﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject loadPanel;

    public void PlayGame()
    {
        loadPanel.SetActive(true);
        AsyncOperation async = SceneManager.LoadSceneAsync("BoardGame");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
