﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TurnController : MonoBehaviour
{
    public static TurnController instance;

    public Player player1;
    public Player player2;
    public Player currentPlayer;

    public Figure figure1;
    public Figure figure2;

    public GameObject hand1;
    public GameObject hand2;

    public Vector2 hand1FightPos;
    public Vector2 hand1WaitPos;
    public Vector2 hand2FightPos;
    public Vector2 hand2WaitPos;

    public Sprite rock;
    public Sprite paper;
    public Sprite scissors;
    public Sprite gun;

    public float handsSpeed;

    public Enemy enemy;

    public UnityAction afterFightAction;
    public UnityAction playerEffect;
    public BoardPlayer currentBoardPlayer;

    public string winMessage;
    public Text boardMessage;

    public string fightMessage;

    bool bossFight = false;
    public GameObject bossBattle;

    public GameObject enemyBattle;
    public Text afterWinEffect;

    public bool canChooseFigure;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Update()
    {
        if (currentPlayer != null && canChooseFigure)
        {
            string code = "";

            if (currentPlayer == player1)
            {
                if (Input.GetKeyDown(KeyCode.A))
                {
                    code = "Rock";
                }
                else if (Input.GetKeyDown(KeyCode.S))
                {
                    code = "Paper";
                }
                else if (Input.GetKeyDown(KeyCode.D))
                {
                    code = "Scissors";
                }
                else if (Input.GetKeyDown(KeyCode.F) && !currentPlayer.gunUsed)
                {
                    code = "Gun";
                }
            }

            if (code != "")
            {
                if (currentPlayer == player1)
                {
                    figure1 = new Figure(code);
                    SetHandFigureSprite(hand1, code);
                    EnemyTurn();
                }
            }
        }
    }

    public void BattleWithEnemy(BoardPlayer boardPlayer, Enemy enemy, UnityAction afterFightAction, UnityAction playerEffect, string winMessage = "", string fightMessage = "")
    {
        this.winMessage = winMessage;
        this.fightMessage = fightMessage;
        this.enemy = enemy;
        this.afterFightAction = afterFightAction;
        this.playerEffect = playerEffect;
        currentBoardPlayer = boardPlayer;
        bossFight = false;

        GameUI.instance.playerName.text = "Gracz #" + boardPlayer.playerIndex;
        GameUI.instance.enemyName.text = "Duszek";

        currentPlayer = player1;
        player1.currentHP = boardPlayer.currentHP;
        player1.maxHP = boardPlayer.maxHP;
        player1.dmg = boardPlayer.dmg;

        player2.currentHP = enemy.maxHP;
        player2.maxHP = enemy.maxHP;
        player2.dmg = enemy.dmg;

        player1.gunUsed = false;
        player2.gunUsed = false;

        Invoke("ShowEnemy", 0.5f);

        Invoke("StartBattle", 2.5f);
    }

    public void BossBattle(BoardPlayer boardPlayer, Enemy enemy, UnityAction afterFightAction, UnityAction playerEffect, string winMessage = "")
    {
        this.winMessage = winMessage;
        this.enemy = enemy;
        this.afterFightAction = afterFightAction;
        this.playerEffect = playerEffect;
        currentBoardPlayer = boardPlayer;
        bossFight = true;

        GameUI.instance.enemyName.text = "Kalmar";
        GameUI.instance.playerName.text = "Gracz #" + boardPlayer.playerIndex;

        currentPlayer = player1;
        player1.currentHP = boardPlayer.currentHP;
        player1.maxHP = boardPlayer.maxHP;
        player1.dmg = boardPlayer.dmg;

        player2.currentHP = enemy.maxHP;
        player2.maxHP = enemy.maxHP;
        player2.dmg = enemy.dmg;

        player1.gunUsed = false;
        player2.gunUsed = false;

        Invoke("ShowBoss", 0.5f);

        Invoke("StartBattle", 2.5f);
    }

    public void ShowBoss()
    {
        bossBattle.SetActive(true);
        AudioManager.instance.PlayClip(AudioManager.instance.bossFightStart);
    }

    public void ShowEnemy()
    {
        enemyBattle.SetActive(true);
        afterWinEffect.text = fightMessage;
        AudioManager.instance.PlayClip(AudioManager.instance.fightStart);
    }

    public void StartBattle()
    {
        AudioManager.instance.fightSource.Play();
        AudioManager.instance.musicSource.volume = 0.01f;

        bossBattle.SetActive(false);
        enemyBattle.SetActive(false);

        hand1.SetActive(true);
        hand2.SetActive(true);
        hand1.transform.localPosition = hand1WaitPos;
        hand2.transform.localPosition = hand2WaitPos;
        GameUI.instance.ActivateFightUI();
        GameUI.instance.UpdatePlayersUI();
        PlayerTurn();
    }

    public void PlayerTurn()
    {
        canChooseFigure = true;
        GameUI.instance.ShowPlayer1Instructions();
        GameUI.instance.SetTurnMessage("Twoja tura");
    }

    public void EnemyTurn()
    {
        canChooseFigure = false;
        GameUI.instance.ShowPlayer1Instructions();
        GameUI.instance.SetTurnMessage("Tura przeciwnika");
        figure2 = new Figure(RandomFigure());
        if (figure2.code == "Gun")
        {
            player2.gunUsed = true;
        }
        SetHandFigureSprite(hand2, figure2.code);
        Invoke("MakeTurn", 0.5f);
    }

    public string RandomFigure()
    {
        List<string> temp = new List<string> { "Rock", "Paper", "Scissors" };
        if (!player2.gunUsed)
        {
            temp.Add("Gun");
        }
        return temp[Random.Range(0, temp.Count)];
    }

    public void MakeTurn()
    {
        GameUI.instance.HideInstructions();
        GameUI.instance.SetTurnMessage("Starcie...");
        StartCoroutine(TurnIE());
    }

    IEnumerator TurnIE()
    {
        StartCoroutine(MoveFromTo(hand1.transform, hand1WaitPos, hand1FightPos, handsSpeed));
        StartCoroutine(MoveFromTo(hand2.transform, hand2WaitPos, hand2FightPos, handsSpeed));
        AudioManager.instance.PlayClip(AudioManager.instance.handSlide);

        yield return new WaitForSeconds(0.4f);

        Player winner = TurnResult();

        if (winner == player1)
        {
            PlayFigureEffect(figure1);
            yield return new WaitForSeconds(1.6f);
            GameUI.instance.SetTurnMessage("Zwyc. Gracz #1");
            StartCoroutine(MoveFromTo(hand2.transform, hand2FightPos, hand2WaitPos, handsSpeed));
            AudioManager.instance.PlayClip(AudioManager.instance.handSlide);
            yield return new WaitForSeconds(0.6f);
            StartCoroutine(MoveFromTo(hand1.transform, hand1FightPos, hand1WaitPos, handsSpeed));
            AudioManager.instance.PlayClip(AudioManager.instance.handSlide);
            yield return new WaitForSeconds(0.6f);
        }
        else if (winner == player2)
        {
            PlayFigureEffect(figure2);
            yield return new WaitForSeconds(1.6f);
            GameUI.instance.SetTurnMessage("Zwyc. " + GameUI.instance.enemyName.text);
            StartCoroutine(MoveFromTo(hand1.transform, hand1FightPos, hand1WaitPos, handsSpeed));
            AudioManager.instance.PlayClip(AudioManager.instance.handSlide);
            yield return new WaitForSeconds(0.6f);
            StartCoroutine(MoveFromTo(hand2.transform, hand2FightPos, hand2WaitPos, handsSpeed));
            AudioManager.instance.PlayClip(AudioManager.instance.handSlide);
            yield return new WaitForSeconds(0.6f);
        }
        else
        {
            yield return new WaitForSeconds(0.8f);
            GameUI.instance.SetTurnMessage("Remis");
            StartCoroutine(MoveFromTo(hand2.transform, hand2FightPos, hand2WaitPos, handsSpeed));
            StartCoroutine(MoveFromTo(hand1.transform, hand1FightPos, hand1WaitPos, handsSpeed));
            AudioManager.instance.PlayClip(AudioManager.instance.handSlide);
            yield return new WaitForSeconds(0.6f);
        }

        NextTurn();
    }

    public Player TurnResult()
    {
        Player turnWinner = null;

        if (figure1.code == figure2.code)
        {
            player1.combo = 0;
            player2.combo = 0;

            if (figure1.code == "Gun" && figure2.code == "Gun")
            {
                player1.gunUsed = true;
                player2.gunUsed = true;
            }

            GameUI.instance.UpdatePlayersUI();
            return null;
        }
        else
        {
            if (figure1.code == "Gun" && figure2.code != "Gun")
            {
                player1.gunUsed = true;
                turnWinner = player1;
            }
            else if (figure1.code != "Gun" && figure2.code == "Gun")
            {
                player2.gunUsed = true;
                turnWinner = player2;
            }
            else if (figure1.code == "Scissors" && figure2.code == "Paper")
            {
                turnWinner = player1;
            }
            else if (figure1.code == "Rock" && figure2.code == "Scissors")
            {
                turnWinner = player1;
            }
            else if (figure1.code == "Paper" && figure2.code == "Rock")
            {
                turnWinner = player1;
            }
            else
            {
                turnWinner = player2;
            }

            if (turnWinner == player1)
            {
                player2.currentHP = Mathf.Clamp(player2.currentHP - player1.dmg, 0, 100);
                GameUI.instance.DamagePlayer2();
                GameUI.instance.UpdatePlayersUI();
                return player1;
            }
            else
            {
                player1.currentHP = Mathf.Clamp(player1.currentHP - player2.dmg, 0, 100);
                GameUI.instance.DamagePlayer1();
                GameUI.instance.UpdatePlayersUI();
                return player2;
            }

        }
    }

    public void EndGame()
    {
        hand1.SetActive(false);
        hand2.SetActive(false);
        GameUI.instance.CloseFightPanel();
        enemy = null;

        AudioManager.instance.fightSource.Stop();
        AudioManager.instance.musicSource.volume = 0.5f;

        if (bossFight && player1.currentHP > 0)
        {
            AudioManager.instance.PlayClip(AudioManager.instance.bossBeaten);
            playerEffect();
        }

        if (player1.currentHP > 0)
        {
            currentBoardPlayer.currentHP = player1.currentHP;
        }

        afterFightAction();

        winMessage = "";
        BoardUI.instance.UpdatePlayersUI();
    }

    public void HideMessage()
    {
        boardMessage.text = "";
    }

    public void NextTurn()
    {
        if(player1.currentHP == 0)
        {
            GameUI.instance.SetTurnMessage("Zostałeś pokonany!");
            currentBoardPlayer.RespawnInHome();
            AudioManager.instance.PlayClip(AudioManager.instance.fightLost);
            Invoke("EndGame", 2.5f);
        }
        else if(player2.currentHP == 0)
        {
            GameUI.instance.SetTurnMessage(GameUI.instance.enemyName.text + " pokonany!");
            if (winMessage != "")
            {
                boardMessage.text = winMessage;
                Invoke("HideMessage", 6.0f);
            }
            if(!bossFight)
            {
                playerEffect();
                AudioManager.instance.PlayClip(AudioManager.instance.fightWon);
            }

            Invoke("EndGame", 2.5f);
        }
        else
        {
            PlayerTurn();
        }
    }

    public void SetHandFigureSprite(GameObject hand, string code)
    {
        if (code == "Gun")
        {
            hand.GetComponent<Image>().sprite = gun;
        }
        else if (code == "Scissors")
        {
            hand.GetComponent<Image>().sprite = scissors;
        }
        else if (code == "Rock")
        {
            hand.GetComponent<Image>().sprite = rock;
        }
        else if (code == "Paper")
        {
            hand.GetComponent<Image>().sprite = paper;
        }
    }

    IEnumerator MoveFromTo(Transform objectToMove, Vector3 a, Vector3 b, float speed)
    {
        float step = (speed / (a - b).magnitude) * Time.fixedDeltaTime;
        a.z = 1;
        b.z = 1;
        float t = 0;
        while (t <= 1.0f)
        {
            t += step;
            objectToMove.GetComponent<RectTransform>().localPosition = Vector3.Lerp(a, b, t);
            yield return new WaitForFixedUpdate();
        }
        objectToMove.GetComponent<RectTransform>().localPosition = b;
    }

    public void PlayFigureEffect(Figure figure)
    {
        if(figure.code == "Gun")
        {
            AudioManager.instance.PlayClip(AudioManager.instance.pistol);
        }
        else if (figure.code == "Rock")
        {
            AudioManager.instance.PlayClip(AudioManager.instance.rock);
        }
        else if (figure.code == "Paper")
        {
            AudioManager.instance.PlayClip(AudioManager.instance.paper);
        }
        else if (figure.code == "Scissors")
        {
            AudioManager.instance.PlayClip(AudioManager.instance.scissors);
        }
    }
}
