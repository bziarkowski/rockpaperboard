﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUI : MonoBehaviour
{
    public static GameUI instance;

    public GameObject fightPanel;
    public GameObject mainMenu;

    public Player player1;
    public Player player2;

    public Color damageColor;
    public Image player1Info;
    public Image player2Info;

    public Slider player1Slider;
    public Text player1Text;
    public Text player1Combo;
    public Slider player2Slider;
    public Text player2Text;
    public Text player2Combo;

    public Text enemyName;
    public Text playerName;

    public GameObject player1Instructions;
    public GameObject player1GunInstruction;

    public GameObject player2Instructions;
    public GameObject player2GunInstruction;

    public Text turnMessage;

    public void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        UpdatePlayersUI();
    }

    public void ActivateFightUI()
    {
        fightPanel.SetActive(true);
        mainMenu.SetActive(false);
    }

    public void ActivateMainMenu()
    {
        fightPanel.SetActive(false);
        mainMenu.SetActive(true);
    }

    public void CloseFightPanel()
    {
        fightPanel.SetActive(false);
    }

    public void UpdatePlayersUI()
    {
        player1Slider.maxValue = player1.maxHP;
        player1Slider.value = player1.currentHP;
        player1Combo.text = "Atk " + player1.dmg;
        player1Text.text = "HP: " + player1.currentHP + "/" + player1.maxHP;

        player2Slider.maxValue = player2.maxHP;
        player2Slider.value = player2.currentHP;
        player2Combo.text = "Atk " + player2.dmg;
        player2Text.text = "HP: " + player2.currentHP + "/" + player2.maxHP;
    }

    public void ShowPlayer1Instructions()
    {
        player2Instructions.SetActive(false);
        player1Instructions.SetActive(true);
        if(player1.gunUsed)
        {
            player1GunInstruction.SetActive(false);
        }
        else
        {
            player1GunInstruction.SetActive(true);
        }
    }

    public void ShowPlayer2Instructions()
    {
        player1Instructions.SetActive(false);
        player2Instructions.SetActive(true);
        if (player2.gunUsed)
        {
            player2GunInstruction.SetActive(false);
        }
        else
        {
            player2GunInstruction.SetActive(true);
        }
    }

    public void HideInstructions()
    {
        player1Instructions.SetActive(false);
        player2Instructions.SetActive(false);
    }

    public void SetTurnMessage(string message)
    {
        turnMessage.text = message;
    }

    public void DamagePlayer1()
    {
        StartCoroutine(DamageIE(player1Info));
    }

    public void DamagePlayer2()
    {
        StartCoroutine(DamageIE(player2Info));
    }

    IEnumerator DamageIE(Image img)
    {
        img.color = damageColor;
        yield return new WaitForSeconds(0.1f);
        img.color = new Color(1, 1, 1, 1);
    }
}
