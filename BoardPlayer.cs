﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class BoardPlayer : MonoBehaviour
{
    //Stats
    public int maxHP;
    public int currentHP;
    public int dmg;
    public int coins;
    public int combo;
    public bool gunUsed;

    public BoardField fieldOn;

    public Player player;

    public float moveSpeed;
    public UnityAction afterMoveAction;
    bool moving;

    public int playerIndex;

    public Text message;

    public BoardField homeField;

    private void Start()
    {
        transform.position = fieldOn.transform.position;
    }

    public void Move(BoardField targetField)
    {
        transform.position = targetField.transform.position;
        AudioManager.instance.PlayClip(AudioManager.instance.boardMove);

        fieldOn.playerOn = null;
        fieldOn = targetField;
        fieldOn.playerOn = this;

        DeactivateBoardFields();

        if (fieldOn.health || fieldOn.attack || fieldOn.coins)
        {
            UnityAction playerEffect = null;
            string winMessage = "";
            string fightMessage = "";
            if (fieldOn.health)
            {
                playerEffect = () => AddHP(2, fieldOn);
                winMessage = "+2 HP dla Gracza #" + playerIndex;
                fightMessage = "+2 HP po wygranej";
            }
            else if (fieldOn.attack)
            {
                playerEffect = () => AddDmg(1, fieldOn);
                winMessage = "+1 Atk dla Gracza #" + playerIndex;
                fightMessage = "+1 Atk po wygranej";
            }
            else if (fieldOn.coins)
            {
                playerEffect = () => AddCoins(10, fieldOn);
                winMessage = "+10 Monet dla Gracza #" + playerIndex;
                fightMessage = "+10 Monet po wygranej";
            }

            TurnController.instance.BattleWithEnemy(this, new Enemy(EnemyHp(), EnemyAttack()), afterMoveAction, playerEffect, winMessage, fightMessage);
        }
        else if(fieldOn.boss)
        {
            UnityAction playerEffect = () => BossBeaten();
            TurnController.instance.BossBattle(this, new Enemy(GameManager.instance.bossHP, GameManager.instance.bossDmg), afterMoveAction, playerEffect, "Boss pokonany przez gracza #" + playerIndex);
        }
        else if(fieldOn.shop)
        {
            Shop.instance.EnterShop(this, afterMoveAction);
        }
        else
        {
            if (fieldOn.home)
            {
                RestoreHP();
            }
            afterMoveAction();
        }
    }

    public int EnemyHp()
    {
        if (currentHP < 5)
        {
            return Random.Range(2, 4);
        }
        else if(currentHP >= 5 && currentHP < 9)
        {
            return Random.Range(3, 6);
        }
        else if (currentHP >= 9)
        {
            return Random.Range(6, 10);
        }
        return 0;
    }

    public void RespawnInHome()
    {
        fieldOn.playerOn = null;
        fieldOn = homeField;
        fieldOn.playerOn = this;
        transform.position = fieldOn.transform.position;
        RestoreHP();
    }

    public int EnemyAttack()
    {
        if (currentHP < 5)
        {
            return 1;
        }
        else if (currentHP >= 5 && currentHP < 9)
        {
            return Random.Range(1, 3);
        }
        else if (currentHP >= 9)
        {
            return Random.Range(2, 4);
        }
        return 1;
    }

    public void BossBeaten()
    {
        GameManager.instance.GameWinMessage(playerIndex);
    }

    public void RestoreHP()
    {
        currentHP = maxHP;
        BoardUI.instance.UpdatePlayersUI();
    }

    public void AddHP(int amount, BoardField field)
    {
        currentHP += amount;
        maxHP += amount;
        field.SetClear();
    }

    public void AddCoins(int amount, BoardField field)
    {
        coins += amount;
        field.SetClear();
    }

    public void AddDmg(int amount, BoardField field)
    {
        dmg += amount;
        field.SetClear();
    }

    public void ActivateBoardFields(Image img, string message)
    {
        GameManager.instance.boardMessage.text = message;

        img.color = new Color(1, 1, 1, 0.2f);
        GetComponent<Image>().color = new Color(1, 1, 1, 1);

        for (int i = 0; i < GameManager.instance.boardFields.Count; i++)
        {
            BoardField field = GameManager.instance.boardFields[i];
            if (fieldOn.boardConnected.Contains(field) && field != fieldOn && field.playerOn == null)
            {
                field.button.interactable = true;
                field.button.onClick.RemoveAllListeners();
                int tempI = i;
                field.button.onClick.AddListener(() => Move(GameManager.instance.boardFields[tempI]));
            }
            else
            {
                field.button.interactable = false;
            }
        }
    }

    public void DeactivateBoardFields()
    {
        for (int i = 0; i < GameManager.instance.boardFields.Count; i++)
        {
            GameManager.instance.boardFields[i].button.interactable = false;
        }
    }
}

