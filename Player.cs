﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int maxHP;
    public int currentHP;
    public int dmg;
    public int combo;

    public bool gunUsed;

    public void Reset()
    {
        currentHP = maxHP;
        combo = 0;
        gunUsed = false;
    }
}
